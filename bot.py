"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2024 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""



# This is the discord bot
import re
import configparser
import os
import asyncio
import discord
import requests
from urllib.parse import urlparse
from io import BytesIO
from discord.ext import commands
import gpt_api as gpt
import comfyui_api
from chatbot_openai_api import BotOpenAIApi


# llm_type options are 'gpt' or 'ollama'
#VALID_LLM_TYPES = ["gpt", "ollama", "local_llama"]

# different keys can activate the same values. I'm tired of begging the users
# to type things exactly... This also helps with routing deprecated LLMs to
# ones that are currently being used/served
VALID_LLM_TYPES = {
    "gpt": "gpt",

    "local_llama": "local_llama",
    "local": "local_llama",
    "llama": "local_llama",
    "local llama": "local_llama",
    "local-llama": "local_llama",
}

config = configparser.ConfigParser()
config.read("config.ini")

API_TOKEN = config["bot"]["bot_token"]
LLM_TYPE = config["bot"]["default_llm_type"]
print(f"LLM_TYPE: {LLM_TYPE}")

PREFIX = config["bot"]["msg_prefix"]
IMAGE_MODEL = config["bot"]["img_model"]


OPENAI_API_BASE_URL = config["openai_api"]["base_url"]
OPENAI_API_API_KEY = config["openai_api"]["api_key"]
OPENAI_API_TEMP = float(config["openai_api"]["temperature"])
OPENAI_API_CONTEXT_LENGTH = int(config["openai_api"]["context_length"])





def clear_chat_sessions(username=None):
    if username:
        print(f"Clearing chat session for {username}")
    else:
        print("Clearing all chat sessions")

    # Look in the root directory for any .chat files and delete those
    for filename in os.listdir(os.getcwd()):
        if filename.endswith(".chat"):
            if username:
                if filename.startswith(username):
                    with open(filename, "w") as file:
                        file.write("")
            else:
                with open(filename, "w") as file:
                    file.write("")



def get_user_llm_type(username):
    filename = f"{username}_llm_type.txt"
    user_llm = None

    if os.path.isfile(filename):
        # File exists so just return its contents
        with open(filename, "r") as file:
            user_llm = file.read().strip()

    else:
        # Doesn't exists so we use the existing LLM_TYPE variable
        # and save as a new llm type for the user
        with open(filename, "w") as file:
            file.write(LLM_TYPE)

        user_llm = LLM_TYPE

    print(f"LLM type for {username} is {user_llm}")
    return user_llm



def set_user_llm_type(username, llm_type):
    filename = f"{username}_llm_type.txt"
    with open(filename, "w") as file:
        file.write(llm_type)








intents = discord.Intents.all()
#intents.members = True
#intents.message_content = True
#client = discord.Client(intents=intents)


# Create instance of bot
bot = commands.Bot(command_prefix='!', intents=intents)
bot.LLM_TYPE = LLM_TYPE  # TODO figure out why this wasn't working when it was just a global variable
#bot.IMAGE_MODEL = IMAGE_MODEL

# Clear all sessions when first starting up. For debugging.
# clear_chat_sessions()

@bot.event
async def on_ready():
    print('We have logged in as {0.user}'.format(bot))

@bot.command()
async def ping(ctx):
    print("ping command was called")
    await ctx.send('Pong!')

@bot.command()
async def hello(ctx):
    print("Hello command was called")
    await ctx.send("Why hello there you beautiful human!")


async def upload_image_url_to_discord(url, message, image_model):
    # Downloads and then re-uploads the image to discord's servers
    # replies to a message
    response = requests.get(url)
    image_bytes = BytesIO(response.content)

    parsed_url = urlparse(url)
    filename = os.path.basename(parsed_url.path)

    discord_file = discord.File(fp=image_bytes, filename=filename)


    try:
        await message.reply(f"({image_model})", file=discord_file)
    except Exception as ex:
        await message.reply(f"There was an error uploading the image URL to Discord:")
        await message.reply(str(ex))
        print(ex)



async def upload_image_files_to_discord(images, message, image_model):
    #import pdb; pdb.set_trace()

    for filename in images:
        with open(filename, 'rb') as image_file:
            discord_file = discord.File(image_file)

            # Attempt to parse a seed out of the filename if it's
            # coming from comfyui
            if image_model == "comfyui":
                file = filename.split('/')[-1]

                file_parts = file.split("_")
                if len(file_parts) >= 4:
                    # There's a seed here!
                    seed_used = file_parts[-3]

                    if seed_used:
                        reply_msg = f"({image_model} seed: {seed_used})"


            else:
                reply_msg = f"({image_model})"



            try:
                os.remove(filename)
                await message.reply(reply_msg, file=discord_file)
            except Exception as ex:
                await message.reply("There was an error uploading image files to Discord:")
                await message.reply(str(ex))
                print(ex)






async def get_image_from_bot_prompt(message, prompt, image_model, image_size=None):
    # This happens when the bot responds with 'OK. Generating image'
    # following that string there will be a prompt, that prompt needs to get
    # forwarded to send_image_prompt and that needs to send a message
    # back to the chat
    #print(f"Sending image prompt to generator:\n'''{prompt}'''\n")
    await bot.change_presence(activity=discord.Game(name="thinking..."))
    try:

        if image_model == "comfyui":
            if message.attachments:
                response = await comfyui_api.send_image_prompt(
                    prompt, message.author.name, message.attachments[0].url)
            else:
                response = await comfyui_api.send_image_prompt(prompt, message.author.name)


        elif image_model == 'dall-e-3':
            response = await gpt.send_image_prompt(prompt, image_model, image_size)
        else:
            await message.reply(f"Unknown image_model '{image_model}'")
            response = None

    except Exception as ex:
        await message.reply(f"There was an error getting the image from the bot prompt:\n{ex}")
        response = None


    # The response is just the url
    await bot.change_presence()

    return response, image_model


    # This is disabled. Now the image will get uploaded to discord's servers
    '''
    try:
        await message.reply(f"{response}")


    except Exception as ex:
        await message.reply(f"There was an error:")
        await message.reply(str(ex))
        print(ex)
    '''





@bot.event
async def on_message(message):
    if message.author == bot.user:
        # prevent infinite feedback loop where the AI goes crazy
        # WARNING can be very entertaining!
        return

    # If it has the prefix or its a dm or an @ then we're good, otherwise
    # don't do anything.
    if not ((message.content.startswith(PREFIX) is True
            or isinstance(message.channel, discord.DMChannel) is True
            or bot.user in message.mentions)):
        return


    # TODO if the message is @ the bot then do not require a prefix


    content = message.content
    if content.startswith("!"):
        content = content[1:].strip()


    if content.lower() in [
        "clear_session", "clear session", "clear-session", "session-clear",
        "session clear", "session_clear",
    ]:
        if bot.LLM_TYPE == "gpt":
            clear_chat_sessions(message.author.name)
        elif bot.LLM_TYPE == "ollama":
            clear_chat_sessions(message.author.name)
        elif bot.LLM_TYPE == "local_llama":
            clear_chat_sessions(message.author.name)

        await message.reply(f"Session cleared for {message.author.name}")

        return


    if content.lower().startswith("change llm to"):
        desired_llm_type = content.lower().split("change llm to")[1].strip().lower()

        if desired_llm_type in VALID_LLM_TYPES:
            set_user_llm_type(message.author.name, VALID_LLM_TYPES[desired_llm_type])

            await message.reply(f"Changed LLM to {desired_llm_type} for {message.author.name}")

        else:
            await message.reply(f"Unknown LLM '{desired_llm_type}'")

        return





    print("\nInput:", content)

    await bot.change_presence(activity=discord.Game(name="thinking..."))
    #status = discord.Activity(type=discord.ActivityType.listening, name="data from the AI hive mind")
    #await bot.change_presence(activity=status)

    user_llm_type = get_user_llm_type(message.author.name)
    try:

        if user_llm_type == 'gpt':
            response = await gpt.send_chat_prompt(message.author, message.content)
        elif user_llm_type == "local_llama":
            openai_sys_prompt = ""
            with open("chatbot_openai_api_prompt.txt", "r") as file:
                openai_sys_prompt = file.read().strip()

            llama = BotOpenAIApi(
                base_url=OPENAI_API_BASE_URL,
                keyfile=OPENAI_API_API_KEY,
                chat_model="Local Llama",
                temp=OPENAI_API_TEMP,
                context_length=OPENAI_API_CONTEXT_LENGTH,
                system_prompt=openai_sys_prompt,
            )

            response = await llama.send_chat_prompt(message.author, message.content)

        else:
            await message.channel.send(f"LLM of '{user_llm_type}' is not recognized.\nValid LLM types are {VALID_LLM_TYPES}")
            return


    except Exception as ex:
        response = f"There was an error connecting to the LLM:\n{ex}"


    #print(f"\nResponse:", response)


    response_chunks = []
    if len(response) >= 1500:
        for i in range(0, len(response), 1500):
            response_chunks.append(response[i:i + 1500])
    else:
        response_chunks.append(response)


    #await message.channel.send(f"{response}")
    try:
        for chunk in response_chunks:
            await message.reply(f"{chunk}")
            # this is so we're not spamming but sending the chunks every second
            await asyncio.sleep(1)

    except Exception as ex:
        await message.reply("There was an error sending the reply:")
        await message.reply(str(ex))
        print(ex)

    await bot.change_presence()

    # If this message contains an ai-generated image prompt, send that
    # to be processed!
    fmt_img_prompt = response.lower()
    if ("generating image" in fmt_img_prompt
            or "aspect ratio" in fmt_img_prompt
            or "image description" in fmt_img_prompt):

        if "generating image" in fmt_img_prompt:
            fmt_img_prompt = fmt_img_prompt.split("generating image")[1]

        fmt_img_prompt = (
            fmt_img_prompt
            .replace("image description:", "")
            .replace("image description...", "")
            .replace("image description", "")
            .replace("...", "")
            .replace("prompt:", "")
            .split("----------")[0]
            .split("(")[0]
            .replace('"', '')
            .replace('</s>', '')
            .strip()
        )



        custom_res = None
        if "Resolution:" in response:
            # Removing the resolution data from the prompt.
            fmt_img_prompt = fmt_img_prompt.split("resolution:")[0].replace("\n", "").strip()


            # Important to split the original response instead of fmt_imag_prompt
            # because that already had the resolution data removed.
            custom_res = (
                response.lower()
                .split("resolution:")[1]
                .split("(")[0]
                .replace("pixels", "")
                .replace("\\", "")
                .replace("'", "")
                .replace("\n", "")
                .replace('</s>', '')
            ).strip()

            if not custom_res:
                custom_res = None


        print("CUSTOM RESOLUTION:", custom_res)
        print(f"FMT_IMAGE_PROMPT:\n'''{fmt_img_prompt}'''\n")


        if user_llm_type == "local_llama":
            images, image_model = await get_image_from_bot_prompt(
                message=message,
                prompt=fmt_img_prompt,
                image_model="comfyui",
                image_size=None,  # Can be null
            )

            if not images:
                await message.reply("Sorry, something went wrong getting your generated image. Please try again.")
            else:
                await upload_image_files_to_discord(
                    images=images,
                    message=message,
                    image_model=image_model
                )

        else:
            image_url, image_model = await get_image_from_bot_prompt(
                message=message,
                prompt=fmt_img_prompt,
                image_model=IMAGE_MODEL,
                image_size=custom_res,  # Can be null
            )

            if image_url:
                await upload_image_url_to_discord(
                    url=image_url,
                    message=message,
                    image_model=image_model)








bot.run(API_TOKEN)






