"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2024 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""




# ComfyUI API functions


import os
import json
import time
import requests
import random
import asyncio
import configparser
from concurrent.futures import ThreadPoolExecutor

#This is the ComfyUI api prompt format.

#If you want it for a specific workflow you can "enable dev mode options"
#in the settings of the UI (gear beside the "Queue Size: ") this will enable
#a button on the UI to save workflows in api format.

#keep in mind ComfyUI is pre alpha software so this format will change a bit.

#this is the one for the default workflow

import comfyui_models

config = configparser.ConfigParser()
config.read("config.ini")



OUTPUT_DIR = config["comfyui"]["output_dir"]
BATCH_SIZE = int(config["comfyui"]["batch_size"])
COMFY_PROMPT_ADDRESS = config["comfyui"]["prompt_address"]
MODEL_NAME = config["comfyui"]["model_name"]
IMG2IMG_DENOISE = float(config["comfyui"]["img2img_denoise"])
IMG2IMG_GUIDANCE = float(config["comfyui"]["img2img_guidance"])


def queue_prompt(prompt_data):
    #import pdb; pdb.set_trace()
    full_prompt = {"prompt": prompt_data}

    response = requests.post(COMFY_PROMPT_ADDRESS, json=full_prompt)

    if not response.ok:
        return f"ERROR: {response.content}"

    else:
        return "OK"




async def send_image_prompt(user_prompt, sender_id, input_filename=None):
    executor = ThreadPoolExecutor(5)
    loop = asyncio.get_event_loop()
    response = await loop.run_in_executor(executor, _blocking_send_image_prompt, user_prompt, sender_id, input_filename)

    return response


def _blocking_send_image_prompt(user_prompt, sender_id, input_filename=None):
    # Async sends the prompt to the comfyui server and then also
    # activates function that will watch the directory and wait for the
    # files to show up...
    aspect = "square"
    if "portrait" in user_prompt.lower():
        aspect = "portrait"
    elif "landscape" in user_prompt.lower():
        aspect = "landscape"



    if MODEL_NAME == "fast":
        comfy_model = comfyui_models.FastModel(
            sender_id=sender_id,
            seed=None,
            positive=user_prompt,
            negative="bad hands, extra fingers, extra limbs",
            batch_size=BATCH_SIZE,
            aspect=aspect
        )
    elif MODEL_NAME == "medium":
        if input_filename:
            comfy_model = comfyui_models.MediumImg2ImgModel(
                sender_id=sender_id,
                seed=None,
                positive=user_prompt,
                negative=None,  # Not needed for flux
                batch_size=1,  # Img2Img only does a single item
                aspect=aspect,
                input_filename=input_filename,
                guidance=IMG2IMG_GUIDANCE,
                denoise=IMG2IMG_DENOISE,
            )

        else:
            comfy_model = comfyui_models.MediumModel(
                sender_id=sender_id,
                seed=None,
                positive=user_prompt,
                negative="bad hands, extra fingers, extra limbs",
                batch_size=BATCH_SIZE,
                aspect=aspect,
            )

    else:
        raise Exception(f"MODEL_NAME '{MODEL_NAME}' is not a known model.")



    prompt = comfy_model.prompt
    queue_prompt(prompt)

    image_files = []
    infinity_block = 0
    # Wait for BATCH_SIZE number of images to show up for this user.
    # once they are found, send the list of image_files back
    while len(image_files) < BATCH_SIZE and infinity_block < 1000:
        for filename in os.listdir(OUTPUT_DIR):
            if os.path.isfile(os.path.join(OUTPUT_DIR, filename)):
                full_path = os.path.join(OUTPUT_DIR, filename)
                if filename.startswith(sender_id) and full_path not in image_files:
                    image_files.append(full_path)
                    print("Image files = ", image_files)


        time.sleep(0.5)
        infinity_block += 1
        #print("infinity_block", infinity_block)



    print("returning image files", image_files)
    return image_files









if __name__ == "__main__":

    #with open("fastmodel.json", "r") as file:
    #    test_prompt = json.load(file)


    fast_model = comfyui_models.FastModel(
        sender_id="dro",
        seed=None,
        positive="Closeup picture of a sports car, brightly colored, driving down a winding road somewhere in Italy.",
        negative="bad hands, extra fingers, extra limbs",
        batch_size=3,
        aspect="square"
    )
    prompt = fast_model.prompt

    #import pdb; pdb.set_trace()
    queue_prompt(prompt)



