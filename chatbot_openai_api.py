"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2024 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""





# Basic Functions for talking to an OpenAI-compatible server
import asyncio
from concurrent.futures import ThreadPoolExecutor


from openai import OpenAI
import tiktoken


import json




def make_messages_string(messages):
    msg_string = ""

    for msg in messages:
        msg_string += msg["content"]


    return msg_string


def trim_messages(messages, context_length):
    # Calculate the number of tokens in this message, compare it to the
    # context_length and remove old messages one by one until under the
    # context_length

    enc = tiktoken.get_encoding("cl100k_base")

    # Parse out all the textual data from the messages and put into
    # the tokenizer to get an idea of the length
    messages_data = make_messages_string(messages)

    messages_total_tokens = enc.encode(messages_data)

    # If we're over the context_length, get how much we are over by
    # the difference, and save that number. It will be used later when we
    # start trimming old messages, we can calculate how much we've trimmed
    # and once that number is equal to or greater than the difference
    # we'll know the messages are within our context window.
    # I think this is more efficient than just trimming old messages and
    # recalculating the total context until its under the limit.
    difference = 0
    if len(messages_total_tokens) > context_length:
        difference = len(messages_total_tokens) - context_length


    trimmed_toks = 0
    while trimmed_toks < difference:
        removed_msg = messages.pop(0)

        removed_toks = len(enc.encode(removed_msg["content"]))

        trimmed_toks += removed_toks


    return messages





class BotOpenAIApi:
    def __init__(self, base_url, keyfile, chat_model, temp, context_length, system_prompt):
        self.CHAT_MODEL = chat_model
        self.N_CONTEXT = context_length
        self.TEMPERATURE = temp

        self.BASE_URL = base_url
        self.API_KEY = keyfile



        self.client = OpenAI(
            api_key=self.API_KEY,
            base_url=self.BASE_URL,
        )

        self.executor = ThreadPoolExecutor(5)  # Adjust the number of threads

        if system_prompt and system_prompt.lower() != "default":
            self.SYSTEM_PROMPT = system_prompt
        else:
            self.SYSTEM_PROMPT = '''
            You are an A.I. assistant chat bot with the ability to create
            image prompts, provide writing assistance, and helpful advice.
            You keep it short, concise and follow your instructions carefully.
            You do not editorialize or add irrelevant information.

            If the user asks for you to generate an image or an image prompt
            follow these instructions:
            - Reply with "Generating image..."
            - On the next line write a creative prompt for the image based on what the user said.
            - Make the image prompt creative and descriptive with high complexity.
            - Reply only with the prompt (this is important) and provide only one prompt.
            - Optionally pick an aspect ratio for the image. Choices are landscape, portrait, square.
            - Do not repeat this message.

            Here is an example:
            User: I need an image of a LEGO pirate ship
            Assistant: Generating image... A majestic, intricately detailed LEGO pirate ship with a long, curved hull, a billowing black and white striped sail, and a distinctive crow's nest at its highest point, set against a warm, sunny background with fluffy white clouds and a light blue sky, inviting the viewer to step into the world of swashbuckling adventure.
        '''






    async def send_chat_prompt(self, session_id, message):
        loop = asyncio.get_event_loop()
        _response = await loop.run_in_executor(self.executor, self._blocking_send_chat_prompt, session_id, message)

        return _response



    def _blocking_send_chat_prompt(self, session_id, message):
        # Send a message to the GPT AI and get a response
        # For now we'll just use a file as the session ID to store the serialized
        # message history
        filename = f"{session_id}_gpt.chat"
        try:
            with open(filename, "r") as file:
                messages = json.loads(file.read())

        except (FileNotFoundError, json.decoder.JSONDecodeError):
            print(f"Could not read history file for session_id: '{session_id}'")
            messages = []


        # Save the newest message to history
        messages.append({"role": "user", "content": message})

        # Update the system role (if needed)
        if self.SYSTEM_PROMPT:
            messages.append(
                {"role": "system", "content": self.SYSTEM_PROMPT}
            )

        messages = trim_messages(messages, self.N_CONTEXT)

        # Send and receive a response from the server
        response = self.client.chat.completions.create(
            model=self.CHAT_MODEL,
            messages=messages,
            temperature=self.TEMPERATURE)


        # Saving the AI's response/answer to the messages and writing out to
        # the history file
        #import pdb; pdb.set_trace()
        fmt_response = response.choices[0].message.content.strip()

        messages.append({
            "role": response.choices[0].message.role,
            "content": fmt_response
        })



        with open(filename, "w") as file:
            file.write(json.dumps(messages))


        # Print out the response
        print("")
        print(fmt_response)

        # nobody cares about tokens
        # Get the number of tokens and show those in the response at the end
        #prompt_tokens = response.usage.prompt_tokens
        #comp_tokens = response.usage.completion_tokens
        #total_tokens = response.usage.total_tokens
        #tokens = f"Prompt: {prompt_tokens}\nComp: {comp_tokens}\nTotal: {total_tokens}"
        #return f"{response.choices[0].message.content}\n----------\n{tokens}"
        return f"{fmt_response}\n(local/{self.CHAT_MODEL})"



