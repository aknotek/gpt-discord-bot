import random


class MediumImg2ImgModel:
    def __init__(self, **kwargs):

        params = [
            "sender_id",
            "seed",
            "positive",
            "negative",
            "batch_size",
            "aspect",
            "input_filename",
            "guidance",
            "denoise",
        ]

        self.SENDER_ID = kwargs["sender_id"]
        self.SEED = kwargs["seed"]
        self.POSITIVE = kwargs["positive"]
        self.NEGATIVE = kwargs["negative"]
        self.BATCH_SIZE = kwargs["batch_size"]
        self.INPUT_FILENAME = kwargs["input_filename"]
        self.GUIDANCE = kwargs["guidance"]
        self.DENOISE = kwargs["denoise"]
        self.ASPECT = kwargs["aspect"]

        for param in params:
            if param not in kwargs:
                raise ValueError(f"Missing required parameter: {param}")


        if not self.SEED:
            self.SEED = random.randint(10**10, 10**11 - 1)




        if self.ASPECT == "portrait":
            self.HEIGHT = 1024
            self.WIDTH = 768
        elif self.ASPECT == "landscape":
            self.HEIGHT = 768
            self.WIDTH = 1024
        elif self.ASPECT == "square":
            self.HEIGHT = 768
            self.WIDTH = 768
        else:
            print(f"self.ASPECT '{self.ASPECT}' not recognized. Using square.")
            self.ASPECT = "square"
            self.HEIGHT = 768
            self.WIDTH = 768

        self.IMG_PREFIX = f"{self.SENDER_ID}_{self.SEED}"

        print("MediumImg2ImgModel - Flux Dev FP8")
        print(f"Sender ID : {self.SENDER_ID}")
        print(f"Seed      : {self.SEED}")
        print(f"Aspect    : {self.ASPECT}")
        print(f"Dimensions: {self.WIDTH} x {self.HEIGHT}")
        print(f"Batch Size: {self.BATCH_SIZE}")
        print(f"Guidance  : {self.GUIDANCE}")
        print(f"Denoise   : {self.DENOISE}")


        # Flux dev FP8 img2img simple (and hopefully fast) workflow
        self.prompt = {
          "3": {
            "inputs": {
              "seed": self.SEED,
              "steps": 10,
              "cfg": 2,
              "sampler_name": "euler",
              "scheduler": "beta",
              "denoise": self.DENOISE,
              "model": [
                "24",
                0
              ],
              "positive": [
                "15",
                0
              ],
              "negative": [
                "16",
                0
              ],
              "latent_image": [
                "23",
                0
              ]
            },
            "class_type": "KSampler",
            "_meta": {
              "title": "KSampler"
            }
          },
          "8": {
            "inputs": {
              "samples": [
                "3",
                0
              ],
              "vae": [
                "11",
                0
              ]
            },
            "class_type": "VAEDecode",
            "_meta": {
              "title": "VAE Decode"
            }
          },
          "11": {
            "inputs": {
              "vae_name": "ae.sft"
            },
            "class_type": "VAELoader",
            "_meta": {
              "title": "Load VAE"
            }
          },
          "15": {
            "inputs": {
              "clip_l": self.POSITIVE,
              "t5xxl": self.POSITIVE,
              "guidance": self.GUIDANCE,
              "clip": [
                "25",
                0
              ]
            },
            "class_type": "CLIPTextEncodeFlux",
            "_meta": {
              "title": "CLIPTextEncodeFlux"
            }
          },
          "16": {
            "inputs": {
              "conditioning": [
                "15",
                0
              ]
            },
            "class_type": "ConditioningZeroOut",
            "_meta": {
              "title": "ConditioningZeroOut"
            }
          },
          "23": {
            "inputs": {
              "pixels": [
                "26",
                0
              ],
              "vae": [
                "11",
                0
              ]
            },
            "class_type": "VAEEncode",
            "_meta": {
              "title": "VAE Encode"
            }
          },
          "24": {
            "inputs": {
              "unet_name": "flux1-dev-fp8.safetensors",
              "weight_dtype": "fp8_e4m3fn"
            },
            "class_type": "UNETLoader",
            "_meta": {
              "title": "Load Diffusion Model"
            }
          },
          "25": {
            "inputs": {
              "clip_name1": "clip_l.safetensors",
              "clip_name2": "t5xxl_fp8_e4m3fn.safetensors",
              "type": "flux"
            },
            "class_type": "DualCLIPLoader",
            "_meta": {
              "title": "DualCLIPLoader"
            }
          },
          "26": {
            "inputs": {
              "mode": "resize",
              "supersample": "false",
              "resampling": "bilinear",
              "rescale_factor": 2,
              "resize_width": self.WIDTH,
              "resize_height": self.HEIGHT,
              "image": [
                "36",
                0
              ]
            },
            "class_type": "Image Resize",
            "_meta": {
              "title": "Image Resize"
            }
          },
          "35": {
            "inputs": {
              "filename_prefix": self.IMG_PREFIX,
              "images": [
                "8",
                0
              ]
            },
            "class_type": "SaveImage",
            "_meta": {
              "title": "Save Image"
            }
          },
          "36": {
            "inputs": {
              "url_or_path": self.INPUT_FILENAME
            },
            "class_type": "LoadImageFromUrlOrPath",
            "_meta": {
              "title": "LoadImageFromUrlOrPath"
            }
          }
        }



class MediumModel:
    def __init__(self, sender_id, seed, positive, negative, batch_size, aspect):
        if not seed:
            self.SEED = random.randint(10**10, 10**11 - 1)
        else:
            self.SEED = seed


        self.SENDER_ID = sender_id
        self.POSITIVE = positive
        self.NEGATIVE = negative
        self.BATCH_SIZE = batch_size

        if aspect == "portrait":
            self.HEIGHT = 1024
            self.WIDTH = 768
        elif aspect == "landscape":
            self.HEIGHT = 768
            self.WIDTH = 1024
        elif aspect == "square":
            self.HEIGHT = 768
            self.WIDTH = 768
        else:
            print(f"Aspect '{aspect}' not recognized. Using square.")
            aspect = "square"
            self.HEIGHT = 768
            self.WIDTH = 768

        self.IMG_PREFIX = f"{self.SENDER_ID}_{self.SEED}"

        print("MediumModel - Flux Dev FP8")
        print(f"Sender ID : {self.SENDER_ID}")
        print(f"Seed      : {self.SEED}")
        print(f"Aspect    : {aspect}")
        print(f"Dimensions: {self.WIDTH} x {self.HEIGHT}")
        print(f"Batch Size: {self.BATCH_SIZE}")


        # Flux dev FP8. Possibly throw in a LORA or Finetune someday
        self.prompt = {
          "3": {
            "inputs": {
              "seed": self.SEED,
              "steps": 10,
              "cfg": 2,
              "sampler_name": "euler",
              "scheduler": "beta",
              "denoise": 1,
              "model": [
                "24",
                0
              ],
              "positive": [
                "15",
                0
              ],
              "negative": [
                "16",
                0
              ],
              "latent_image": [
                "37",
                0
              ]
            },
            "class_type": "KSampler",
            "_meta": {
              "title": "KSampler"
            }
          },
          "8": {
            "inputs": {
              "samples": [
                "3",
                0
              ],
              "vae": [
                "11",
                0
              ]
            },
            "class_type": "VAEDecode",
            "_meta": {
              "title": "VAE Decode"
            }
          },
          "11": {
            "inputs": {
              "vae_name": "ae.sft"
            },
            "class_type": "VAELoader",
            "_meta": {
              "title": "Load VAE"
            }
          },
          "15": {
            "inputs": {
              "clip_l": self.POSITIVE,
              "t5xxl": self.POSITIVE,
              "guidance": 4,
              "clip": [
                "25",
                0
              ]
            },
            "class_type": "CLIPTextEncodeFlux",
            "_meta": {
              "title": "CLIPTextEncodeFlux"
            }
          },
          "16": {
            "inputs": {
              "conditioning": [
                "15",
                0
              ]
            },
            "class_type": "ConditioningZeroOut",
            "_meta": {
              "title": "ConditioningZeroOut"
            }
          },
          "24": {
            "inputs": {
              "unet_name": "flux1-dev-fp8.safetensors",
              "weight_dtype": "fp8_e4m3fn"
            },
            "class_type": "UNETLoader",
            "_meta": {
              "title": "Load Diffusion Model"
            }
          },
          "25": {
            "inputs": {
              "clip_name1": "clip_l.safetensors",
              "clip_name2": "t5xxl_fp8_e4m3fn.safetensors",
              "type": "flux"
            },
            "class_type": "DualCLIPLoader",
            "_meta": {
              "title": "DualCLIPLoader"
            }
          },
          "35": {
            "inputs": {
              "filename_prefix": self.IMG_PREFIX,
              "images": [
                "8",
                0
              ]
            },
            "class_type": "SaveImage",
            "_meta": {
              "title": "Save Image"
            }
          },
          "37": {
            "inputs": {
              "width": self.WIDTH,
              "height": self.HEIGHT,
              "batch_size": 1
            },
            "class_type": "EmptyLatentImage",
            "_meta": {
              "title": "Empty Latent Image"
            }
          }
        }










class FastModel:
    def __init__(self, sender_id, seed, positive, negative, batch_size, aspect):
        if not seed:
            self.SEED = random.randint(10**10, 10**11 - 1)
        else:
            self.SEED = seed


        self.SENDER_ID = sender_id
        self.POSITIVE = positive
        self.NEGATIVE = negative
        self.BATCH_SIZE = batch_size

        if aspect == "portrait":
            self.HEIGHT = 1024
            self.WIDTH = 768
        elif aspect == "landscape":
            self.HEIGHT = 768
            self.WIDTH = 1024
        elif aspect == "square":
            self.HEIGHT = 768
            self.WIDTH = 768
        else:
            print(f"Aspect '{aspect}' not recognized. Using square.")
            aspect = "square"
            self.HEIGHT = 768
            self.WIDTH = 768

        self.IMG_PREFIX = f"{self.SENDER_ID}_{self.SEED}"

        print("FastModel - Flux Schnell")
        print(f"Sender ID : {self.SENDER_ID}")
        print(f"Seed      : {self.SEED}")
        print(f"Aspect    : {aspect}")
        print(f"Dimensions: {self.WIDTH} x {self.HEIGHT}")
        print(f"Batch Size: {self.BATCH_SIZE}")


        # Flux schnell 6 steps just the base model and it's very impressive!
        self.prompt = {
          "5": {
            "inputs": {
              "width": self.WIDTH,
              "height": self.HEIGHT,
              "batch_size": self.BATCH_SIZE
            },
            "class_type": "EmptyLatentImage",
            "_meta": {
              "title": "Empty Latent Image"
            }
          },
          "6": {
            "inputs": {
              "text": self.POSITIVE,
              "clip": [
                "11",
                0
              ]
            },
            "class_type": "CLIPTextEncode",
            "_meta": {
              "title": "CLIP Text Encode (Prompt)"
            }
          },
          "8": {
            "inputs": {
              "samples": [
                "13",
                0
              ],
              "vae": [
                "10",
                0
              ]
            },
            "class_type": "VAEDecode",
            "_meta": {
              "title": "VAE Decode"
            }
          },
          "10": {
            "inputs": {
              "vae_name": "ae.sft"
            },
            "class_type": "VAELoader",
            "_meta": {
              "title": "Load VAE"
            }
          },
          "11": {
            "inputs": {
              "clip_name1": "t5xxl_fp8_e4m3fn.safetensors",
              "clip_name2": "clip_l.safetensors",
              "type": "flux"
            },
            "class_type": "DualCLIPLoader",
            "_meta": {
              "title": "DualCLIPLoader"
            }
          },
          "12": {
            "inputs": {
              "unet_name": "flux1-schnell.sft",
              "weight_dtype": "fp8_e4m3fn"
            },
            "class_type": "UNETLoader",
            "_meta": {
              "title": "Load Diffusion Model"
            }
          },
          "13": {
            "inputs": {
              "noise": [
                "25",
                0
              ],
              "guider": [
                "22",
                0
              ],
              "sampler": [
                "16",
                0
              ],
              "sigmas": [
                "17",
                0
              ],
              "latent_image": [
                "5",
                0
              ]
            },
            "class_type": "SamplerCustomAdvanced",
            "_meta": {
              "title": "SamplerCustomAdvanced"
            }
          },
          "16": {
            "inputs": {
              "sampler_name": "euler"
            },
            "class_type": "KSamplerSelect",
            "_meta": {
              "title": "KSamplerSelect"
            }
          },
          "17": {
            "inputs": {
              "scheduler": "beta",
              "steps": 5,
              "denoise": 1,
              "model": [
                "12",
                0
              ]
            },
            "class_type": "BasicScheduler",
            "_meta": {
              "title": "BasicScheduler"
            }
          },
          "22": {
            "inputs": {
              "model": [
                "12",
                0
              ],
              "conditioning": [
                "6",
                0
              ]
            },
            "class_type": "BasicGuider",
            "_meta": {
              "title": "BasicGuider"
            }
          },
          "25": {
            "inputs": {
              "noise_seed": self.SEED
            },
            "class_type": "RandomNoise",
            "_meta": {
              "title": "RandomNoise"
            }
          },
          "27": {
            "inputs": {
              "filename_prefix": self.IMG_PREFIX,
              "images": [
                "8",
                0
              ]
            },
            "class_type": "SaveImage",
            "_meta": {
              "title": "Save Image"
            }
          }
        }









