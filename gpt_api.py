"""

SPDX-License-Identifier: AGPL-3.0-or-later
Copyright (C) 2024 Andrew Knotek <arknotek@pm.me>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""





# Talk to openai
import asyncio
from decimal import Decimal
import configparser
from concurrent.futures import ThreadPoolExecutor


from openai import OpenAI


import json




config = configparser.ConfigParser()
config.read("config.ini")
CHAT_MODEL = config["chatgpt"]["chat_model"]
IMAGE_QUALITY = config["chatgpt"]["image_quality"]
IMAGE_SIZE = config["chatgpt"]["image_size"]
IMAGE_SIZE_OPTIONS = config["chatgpt"]["image_size_options"].split(",")
IMAGE_N = int(config["chatgpt"]["image_n"])
MSG_LIMIT = int(config["chatgpt"]["msg_limit"])
TEMPERATURE = float(config["chatgpt"]["temperature"])
API_KEY = config["chatgpt"]["chatgpt_api_key"]



PERSONALITY = ""
with open("chatgpt_system_prompt.txt", "r") as file:
    PERSONALITY = file.read().strip()



SYSTEM_CONTENT = f'''
    {PERSONALITY}

    If the user wants an image to be generated reply normally then add
    """Generating image..."""
    On the next line write a prompt for the image based on what the user said.
    Your response will be fed into a image generator.

    If the user requests a specific resolution, at the bottom """\nResolution: """ and
    then add the closest resolution in this list: [{", ".join(IMAGE_SIZE_OPTIONS)}]
    otherwise feel free to pick the best one for the job.

    Your "llm type" is "gpt".
    '''




client = OpenAI(api_key=API_KEY)
#model_list = client.models.list()

executor = ThreadPoolExecutor(5)  # Adjust the number of threads



async def send_image_prompt(message, image_model, image_size=None):
    if not image_size:
        image_size = IMAGE_SIZE

    loop = asyncio.get_event_loop()
    response = await loop.run_in_executor(executor, _blocking_send_image_prompt, message, image_model, image_size)
    return response



def _blocking_send_image_prompt(message, image_model, image_size):
    response = client.images.generate(
        model=image_model,
        prompt=message,
        size=image_size,
        quality=IMAGE_QUALITY,
        n=IMAGE_N,
    )

    image_url = response.data[0].url

    return image_url



async def send_chat_prompt(session_id, message):
    loop = asyncio.get_event_loop()
    response = await loop.run_in_executor(executor, _blocking_send_chat_prompt, session_id, message)

    return response



def _blocking_send_chat_prompt(session_id, message):
    # Send a message to the GPT AI and get a response
    # For now we'll just use a file as the session ID to store the serialized
    # message history
    filename = f"{session_id}_gpt.chat"
    try:
        with open(filename, "r") as file:
            messages = json.loads(file.read())

    except (FileNotFoundError, json.decoder.JSONDecodeError):
        print(f"Could not read history file for session_id: '{session_id}'")
        messages = []



    # Save the newest message to history
    messages.append({"role": "user", "content": message})

    # Update the system role (if needed)
    if SYSTEM_CONTENT:
        messages.append(
            {"role": "system", "content": SYSTEM_CONTENT}
        )



    # Send and receive a response from the server
    # Only sending the last X messages to try to keep tokens down until
    # better code can be written
    response = client.chat.completions.create(
        model=CHAT_MODEL,
        messages=messages[-MSG_LIMIT:],
        temperature=TEMPERATURE)


    # Saving the answer to the messages and writing out to the history file
    messages.append({
        "role": response.choices[0].message.role,
        "content": response.choices[0].message.content.replace('</s>', '')
    })

    with open(filename, "w") as file:
        file.write(json.dumps(messages[-MSG_LIMIT:]))


    # Print out the chat gpt response
    print("")
    print(response.choices[0].message.content.replace('</s>', ''))

    # nobody cares about tokens
    # Get the number of tokens and show those in the response at the end
    #prompt_tokens = response.usage.prompt_tokens
    #comp_tokens = response.usage.completion_tokens
    #total_tokens = response.usage.total_tokens
    #tokens = f"Prompt: {prompt_tokens}\nComp: {comp_tokens}\nTotal: {total_tokens}"
    #return f"{response.choices[0].message.content}\n----------\n{tokens}"
    return f"{response.choices[0].message.content}\n\n(gpt/{CHAT_MODEL})"





